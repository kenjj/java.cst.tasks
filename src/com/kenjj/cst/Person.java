package com.kenjj.cst;

/**
 * Created by Giorgi on 18.09.2015.
 */
interface  IPerson{

}

public class Person implements  IPerson{
    private String FirstName;
    private String LastName;
    private int Age;

    public Person(String firstName, String lastname, int age){
        this.FirstName = firstName;
        this.LastName = lastname;
        this.Age = age;
    }
}

