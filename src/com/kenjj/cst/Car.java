package com.kenjj.cst;

/**
 * Created by Giorgi on 18.09.2015.
 */

interface ICar{
    public int getFreeSeats();
    public void putPerson(Person person);
}

public class Car implements ICar{
    private int MaxSeats;
    private int CurrPersons = 0;
    private String Model;

    public Car(String model, int maxSeats){
        this.Model = model;
        this.MaxSeats = maxSeats;
    }
    public int getFreeSeats(){
        return MaxSeats-CurrPersons;
    }
    public void putPerson(Person person){
        if(this.CurrPersons<this.MaxSeats)
             this.CurrPersons++;
        else
            System.out.println("There is No Place in car");
    }

}


