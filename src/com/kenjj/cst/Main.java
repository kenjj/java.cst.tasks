package com.kenjj.cst;


public class Main {

    public static void main(String[] args) {
        Car bmw = new Car("bmw",4);
        Person p1 = new Person("giorgi","chkhaidze",15);
        Person p2 = new Person("giorgi","chkhaidze",15);
        Person p3 = new Person("giorgi","chkhaidze",15);
        bmw.putPerson(p1);
        bmw.putPerson(p2);
        bmw.putPerson(p3);

        System.out.println(bmw.getFreeSeats());
    }
}


